(package-initialize)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))

(scroll-bar-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(global-linum-mode t)
(show-paren-mode 1)
(global-hl-line-mode t)

(autoload 'skribe-mode "skribe.el" "Skribe mode." t)
(autoload 'forth-mode "gforth.el" "forth mode." t)

(defvar blink-cursor-colors (list "#92c48f" "#6785c5" "#be369c" "#d9ca65" "#ff0000" "#00ff00" "#ffffff"))
(setq blink-cursor-count 0)

(defun blink-cursor-timer-function ()
  (when (not (internal-show-cursor-p))
    (when (>= blink-cursor-count (length blink-cursor-colors))
      (setq blink-cursor-count 0))
    (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
    (setq blink-cursor-count (+ 1 blink-cursor-count)))
  (internal-show-cursor nil (not (internal-show-cursor-p))))

;;;(require 'main-line)
(powerline-default-theme)
(require 'auto-complete-config)
(ac-config-default)

(load (expand-file-name "~/.quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")

(global-set-key (kbd "M-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "M-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "M-<down>") 'shrink-window)
(global-set-key (kbd "M-<up>") 'enlarge-window)

(require 'buffer-move)
(global-set-key (kbd "<C-S-up>") 'buf-move-up)
(global-set-key (kbd "<C-S-down>") 'buf-move-down)
(global-set-key (kbd "<C-S-left>") 'buf-move-left)
(global-set-key (kbd "<C-S-right>") 'buf-move-right)

(add-hook 'lisp-mode-hook
          '(lambda ()
            (local-set-key (kbd "RET") 'newline-and-indent)))

(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
            (local-set-key (kbd "RET") 'newline-and-indent)))

(defun toggle-fullscreen (&optional f)
  (interactive)
  (let ((current-value (frame-parameter nil 'fullscreen)))
    (set-frame-parameter nil 'fullscreen
                         (if (equal 'fullboth current-value)
                             (if (boundp 'old-fullscreen) old-fullscreen nil)
                           (progn (setq old-fullscreen current-value)
                                  'fullboth)))))

(global-set-key [f11] 'toggle-fullscreen)

(cua-mode 1)
(helm-mode -1)

(autoload 'enable-paredit-mode "paredit" "turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
(add-hook 'javascript-mode-hook #'enable-paredit-mode)

(add-to-list 'load-path "~/.emacs.d/lisp")
(autoload 'squirrel-mode "squirrel-mode"
  "Major mode for editing Squirrel code." t)
(add-to-list 'auto-mode-alist '("\\.nut$" . squirrel-mode))
(autoload 'modula-3-mode "modula3"
  "Major mode for editing Modula-3 code." t)

(require 'modula3)
(add-to-list 'auto-mode-alist '("\\.ig$" . modula-3-mode))
(add-to-list 'auto-mode-alist '("\\.mg$" . modula-3-mode))
(add-to-list 'auto-mode-alist '("\\.i3$" . modula-3-mode))
(add-to-list 'auto-mode-alist '("\\.m3$" . modula-3-mode))

(require 'dirtree)
(require 'pretty-mode)
(global-pretty-mode 1)

(autoload 'oberon-2-mode "oberon2"
  "Major mode for editing Oberon-2 code." t)
(add-to-list 'auto-mode-alist '("\\.Mod$" . oberon-2-mode))

(load "/home/dwhyte/quicklisp/.clhs-use-local.el" t)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(defun slime-redirect-trace-output ()
  (interactive)
  (let ((buffer (get-buffer-create (slime-buffer-name :trace))))
    (with-current-buffer buffer
      (let ((marker (copy-marker (buffer-size)))
            (target (incf slime-last-output-target-id)))
        (puthash target marker slime-output-target-to-marker)
        (slime-eval '(swank:redirect-trace-output ,target))))
    (pop-to-buffer buffer)))

(defmacro with-face (str &rest properties)
    `(propertize ,str 'face (list ,@properties)))

(defun sl/make-header ()
  (let* ((sl/full-header (abbreviate-file-name buffer-file-name))
         (sl/header (file-name-directory sl/full-header))
         (sl/drop-str "[...]"))
    (if (> (length sl/full-header)
           (window-body-width))
        (if (> (length sl/header)
               (window-body-width))
            (progn
              (concat (with-face sl/drop-str
                                 :background "blue"
                                 :weight 'bold)
                      (with-face (substring sl/header
                                            (+ (- (length sl/header)
                                                  (window-body-width))
                                               (length sl/drop-str))
                                            (length sl/header))
                                 :weight 'bold)))
          (concat (with-face sl/header
                             :foreground "#8fb28f"
                             :weight 'bold)))
      (concat (with-face sl/header
                         :weight 'bold
                         :foreground "#8fb28f")
              (with-face (file-name-nondirectory buffer-file-name)
                         :weight 'bold)))))

(defun sl/display-header ()
  (setq header-line-format
        '("" 
          (:eval (if (buffer-file-name)
                     (sl/make-header)
                   "%b")))))

(add-hook 'buffer-list-update-hook
          'sl/display-header)

(custom-set-variables
 '(ansi-color-faces-vector [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector (vector "#eaeaea" "#d54e53" "DarkOliveGreen3" "#e7c547" "DeepSkyBlue1" "#c397d8" "#70c0b1" "#181a26"))
 '(ansi-term-color-vector [unspecified "#081724" "#ff694d" "#68f6cb" "#fffe4e" "#bad6e2" "#afc0fd" "#d2f1ff" "#d3f9ee"] t)
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes (quote (underwater)))
 '(custom-safe-themes (quote ("e26780280b5248eb9b2d02a237d9941956fc94972443b0f7aeec12b5c15db9f3"
			      "53e29ea3d0251198924328fd943d6ead860e9f47af8d22f0b764d11168455a8e"
			      "7c82c94f0d0a4bbdb7cda2d0587f4ecbbe543f78f2599e671af4d74cea1ae698"
			      "ee6081af57dd389d9c94be45d49cf75d7d737c4a78970325165c7d8cb6eb9e34"
			      "a774c5551bc56d7a9c362dca4d73a374582caedb110c201a09b410c0ebbb5e70"
			      "c739f435660ca9d9e77312cbb878d5d7fd31e386a7758c982fa54a49ffd47f6e"
			      "297063d0000ca904abb446944398843edaa7ef2c659b7f9087d724bf6f8c1d1f"
			      "1989847d22966b1403bab8c674354b4a2adf6e03e0ffebe097a6bd8a32be1e19"
			      "bf648fd77561aae6722f3d53965a9eb29b08658ed045207fe32ffed90433eb52"
			      "7d4d00a2c2a4bba551fcab9bfd9186abe5bfa986080947c2b99ef0b4081cb2a6"
			      "e80a0a5e1b304eb92c58d0398464cd30ccbc3622425b6ff01eea80e44ea5130e"
			      "572caef0c27b100a404db8d540fd5b31397f90ab660ef5539ff0863ff9bee26a"
			      "f0ea6118d1414b24c2e4babdc8e252707727e7b4ff2e791129f240a2b3093e32"
			      "758da0cfc4ecb8447acb866fb3988f4a41cf2b8f9ca28de9b21d9a68ae61b181"
			      default)))
 '(fci-rule-character-color "#202020")
 '(fci-rule-color "#14151E")
 '(fringe-mode 10 nil (fringe))
 '(inhibit-startup-screen t)
 '(linum-format "%3i ")
 '(main-line-color1 "#222232")
 '(main-line-color2 "#333343")
 '(main-line-separator-style (quote chamfer))
 '(minimap-always-recenter t)
 '(minimap-buffer-name-prefix "*MiniMap*")
 '(minimap-dedicated-window nil)
 '(minimap-hide-fringes t)
 '(minimap-recenter-type (quote relative))
 '(minimap-resizes-buffer t)
 '(powerline-color1 "#222232")
 '(powerline-color2 "#333343")
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map (quote ((20 . "#d54e53")
				 (40 . "goldenrod")
				 (60 . "#e7c547")
				 (80 . "DarkOliveGreen3")
				 (100 . "#70c0b1")
				 (120 . "DeepSkyBlue1")
				 (140 . "#c397d8")
				 (160 . "#d54e53")
				 (180 . "goldenrod")
				 (200 . "#e7c547")
				 (220 . "DarkOliveGreen3")
				 (240 . "#70c0b1")
				 (260 . "DeepSkyBlue1")
				 (280 . "#c397d8")
				 (300 . "#d54e53")
				 (320 . "goldenrod")
				 (340 . "#e7c547")
				 (360 . "DarkOliveGreen3"))))
 '(vc-annotate-very-old-color nil)
 '(window-resize-pixelwise t))

(custom-set-faces
 '(default ((t (:family "Bitstream Vera Sans Mono"
		:foundry "bitstream"
		:slant normal
		:weight normal
		:height 97
		:width normal))))
 '(hl-line ((t (:stipple nil
		:background "#18374f"
		:underline nil)))))
